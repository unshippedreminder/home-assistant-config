@service
def search_missing_movies():
    """ 
    Force Radarr to search for monitored movies that are missing.
    """
    import requests

    radarr_host = pyscript.config['search_missing_movies']['radarr_host']
    radarr_port = pyscript.config['search_missing_movies']['radarr_port']
    radarr_api = pyscript.config['search_missing_movies']['radarr_api']

    post_url = "http://" + radarr_host + ":" + str(radarr_port) + "/api/command" #?apiKey=" + radarr_api
    post_data = "{'name': 'missingMoviesSearch','filterKey': 'monitored','filterValue': 'true'}"
    post_headers = {
        'Content-Type': 'application/json',
    }
    post_params = (
        ('apiKey', radarr_api),
    )

    # task.executor(func, *args, **kwargs) is needed to avoid IO inside the HASS main loop, which would block execution in the main loop.
    # See https://hacs-pyscript.readthedocs.io/en/stable/reference.html?highlight=access%20hass%20global%20variable#avoiding-event-loop-i-o
    post_response = task.executor(requests.post, post_url, data = post_data, headers = post_headers, params = post_params)

# Working curl command:
# curl -X POST -H "Content-Type: application/json; charset=utf-8" http://radarr.lan:7878/api/command\?apiKey\=b2c1523a553d425dbc912c8ad95f1400 --data '{"name":"missingMoviesSearch","filterKey":"monitored","filterValue":"true"}'